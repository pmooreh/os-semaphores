#include "thread.h"
#include "stdint.h"
#include "debug.h"
#include "pic.h"
#include "heap.h"
#include "machine.h"
#include "queue.h"
#include "locks.h"

static SimpleQueue<Thread>* readyQ;
static SimpleQueue<Thread>* reaperQ;
static SpinLock* contextLock;

static Thread* activeThread = nullptr;
static int nextId = 0;

void Thread::init(void) {
    contextLock = new SpinLock();
    readyQ = new SimpleQueue<Thread>();
    reaperQ = new SimpleQueue<Thread>();
    Thread* firstThread = new Thread(getThenIncrement(&nextId,1));
    activeThread = firstThread;
}

Thread* Thread::current(void) {
    return activeThread;
}

extern "C" void contextSwitch(uint32_t newEsp, uint32_t *oldEsp);

int threadId(void) {
    return (activeThread == 0) ? -1 : activeThread->id;
}

static void reaper() {
    while (true) {
        bool was = contextLock->lock();
        if (!was) {
            /* Don't want to touch the heap with interrupts disabled */
            contextLock->unlock(was);
            return;
        }
        Thread* t = reaperQ->remove();
        contextLock->unlock(was);
        if (t == nullptr) return;
        delete t;
    }
}

/* Switch from the current thread after it has been
   placed on a queue */
/* holding contextLock */
void threadSwitch(int was) {

    Thread* old = activeThread;

    Thread* next = readyQ->remove();

    if (next == nullptr) Debug::panic("no threads\n");

    if (old != next) {
        activeThread = next;
        contextSwitch(next->esp,&old->esp);
    }

    contextLock->unlock(was);
}

/*
 * Add the active thread to the given queue then release
 * the given lock.
 *
 * Pre-conditions:
 *    - lock is held (in order to prevent the active thread
 *      from being added to the ready queue
 *    - interrupts are disabled
 */
void threadBlock(SpinLock *lock, bool was) {
    contextLock->lock();         /* Get the context lock */
    lock->unlock(false);         /* Unlock the queue while holding the context
                                    lock */
    threadSwitch(was);
}

void threadYield(void) {
    reaper();
    bool was = contextLock->lock();
    readyQ->add(activeThread);
    threadSwitch(was);
}

void threadExit(void) {
    reaper();
    bool was = contextLock->lock();
    reaperQ->add(activeThread);
    threadSwitch(was);
}

void threadReady(Thread* t) {
    bool was = contextLock->lock();
    readyQ->add(t);
    contextLock->unlock(was);
}

static void threadEntry(ThreadFunc func, void* arg) {
    contextLock->unlock(true);
    reaper();
    func(arg);
    threadExit();
    Debug::panic("should never get here\n");
}

int threadCreate(ThreadFunc func, void* arg) {
    int id = getThenIncrement(&nextId,1);
    reaper();

    Thread* t = new Thread(id);
    if (t == nullptr) return -1;
        
    t->push((uint32_t) arg);
    t->push((uint32_t) func);
    t->push(0);
    t->push((uint32_t) threadEntry);
    for (int i=0; i<7; i++) t->push(0); // regs
    
    threadReady(t);
    return id;
}

