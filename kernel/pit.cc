#include "pit.h"
#include "debug.h"
#include "machine.h"
#include "idt.h"
#include "pic.h"
#include "thread.h"

#define FREQ 1193182

#define IRQ 0

void pitInit(uint32_t hz) {

     idtAdd(picBase+IRQ, (uint32_t) pitAsmHandler);

     uint32_t d = FREQ / hz;

     Debug::printf("pitInit freq %d\n",hz);
     
     if ((d & 0xffff) != d) {
         Debug::printf("pitInit invalid divider %d\n",d);
         d = 0xffff;
     }
     Debug::printf("pitInit divider %d\n",d);
     hz = FREQ / d;
     Debug::printf("initInit actual freq\n",hz);
     pit_do_init(d);
}

extern "C" void pitHandler() {
    picEoi(IRQ);
    threadYield();
}
