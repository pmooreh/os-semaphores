#include "idt.h"
#include "stdint.h"

extern uint32_t idt[];
extern uint32_t kernelCS;

void idtInit(void) {
}

void idtAdd(int index, uint32_t handler) {
    int i0 = 2 * index;
    int i1 = i0 + 1;

    idt[i0] = (kernelCS << 16) + (handler & 0xffff);
    idt[i1] = (handler & 0xffff0000) | 0x8e00;
}
