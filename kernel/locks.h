#ifndef _LOCKS_H_
#define _LOCKS_H_

#include "stdint.h"
#include "pic.h"
#include "queue.h"
#include "thread.h"

inline uint32_t cmpxchg(volatile uint32_t *p,
    uint32_t cmpVal, uint32_t setVal) {
  uint32_t res;
  __asm__ volatile( "lock cmpxchgl %2,%1"
    : "=a" (res), "+m" (*p)
    : "r" (setVal), "0" (cmpVal)
    : "memory", "cc");
  return res;
}
    
class SpinLock {
    volatile uint32_t flag;
public:
    SpinLock() : flag(0) {}

    inline bool tryLock() {
        uint32_t oldValue = cmpxchg(&flag,0,1);
        return (oldValue == 0);
    }

    inline bool lock() {
        while (true) {
            bool was = picDisable();
            if (tryLock()) return was;
            picRestore(was);
        }
    }

    inline void unlock(bool was) {
        flag = 0;
        picRestore(was);
    }
};

class Semaphore {
    uint32_t count;
	// also needs a thread queue
	SimpleQueue<Thread> *q;
	// and a spin lock
	SpinLock *spinLock;
public:
    Semaphore(uint32_t count) : count(count), q(new SimpleQueue<Thread>()), spinLock(new SpinLock()) {}

    void down() {
		// lock the semaphore
		bool was = spinLock->lock();
		// try to decrease the counter
		if (count > 0) {
			count--;
            // we're done. unlock this semaphore!
            spinLock->unlock(was);
		}
        // otherwise, 
		else {
			// queue up
			q->add(Thread::current());

            // block this thread
            threadBlock(spinLock, was);
            // we're done. unlock this semaphore!
            spinLock->unlock(was);
		}
    }

    void up() {
        // lock the semaphore
        bool was = spinLock->lock();
        // remove element from q
        Thread* thread = q->remove();
        // if it isn't null, add to ready queue? thread ready to the rescue!
        if (thread != nullptr) {
            threadReady(thread);
        }
        // otherwise, increment count
        else {
            count++;
        }
        // we're done. unlock the semaphore!
        spinLock->unlock(was);
    }
};

class BlockingLock {
	// has a semaphore
	Semaphore* sem;
public:
    BlockingLock() : sem(new Semaphore(1)) {}
    void lock() {
		sem->down();
	}
    void unlock() {
		sem->up();
	}
};

class Event {
	Semaphore* sem;
public:
    Event() : sem(new Semaphore(0)) {}

    void wait() {
		// sem down to add thread
		sem->down();
		
		// SO NOW: when the thread returns here, sem up?
		sem->up();
	}
    void signal() {
		// a single sem up to set off the sem up cascade!
		sem->up();
	}
};

class Barrier {
    uint32_t togo;
    BlockingLock* lock;
	Semaphore* sem;
public:
    Barrier(uint32_t n) : togo(n), lock(new BlockingLock()), sem(new Semaphore(0)) {}

 //    void sync() {
	// 	if (togo > 1) {
	// 		sem->down();
 //            togo--;

	// 		// once you return, sem up to release the next thread
	// 		sem->up();
	// 	}
	// 	else {
	// 		// no looping, so what happens here?!
	// 		sem->up();
	// 	}
	// }

    void sync() {
        // lock it down!
        lock->lock();

        // check togo
        if (togo > 1) {
            togo--;
            lock->unlock();
            sem->down();

            sem->up();
        }

        else {
            lock->unlock();
            sem->up();
        }

    }
};

class BoundedBuffer {
    int *data;
    int n;
    int getIndex;
    int putIndex;
    BlockingLock* lock;
	Semaphore* putCount;
	Semaphore* emptyCount;

public:

    BoundedBuffer(int n) : data(new int[n]), n(n), getIndex(0), putIndex(0), lock(new BlockingLock()), putCount(new Semaphore(0)), emptyCount(new Semaphore(n)) {}
    ~BoundedBuffer() {
        if (data != nullptr) {
            delete []data;
            data = nullptr;
        }
   }

    void put(int v) {
        emptyCount->down();

        // insert element
        lock->lock();
        data[putIndex] = v;
        putIndex = (putIndex + 1) % n;
        lock->unlock();

        putCount->up();
	} 

    int get(void) {
        int value;

        putCount->down();

        // grab element
        lock->lock();
        value = data[getIndex];
        getIndex = (getIndex + 1) % n;
        lock->unlock();

        
        emptyCount->up();

        return value;
    }

};

#endif
