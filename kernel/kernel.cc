#include "stdint.h"
#include "debug.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "machine.h"
#include "heap.h"
#include "random.h"
#include "config.h"
#include "u8250.h"
#include "locks.h"

Barrier *done = nullptr;

#define M 4

int mCount = 0;
int fCount = 0;

void f0(void* arg) {
    int* p = (int*) arg;

    //Debug::printf("start %d\n",*p);

    void* last[M];

    for (int i=0; i<M; i++) {
        last[i] = 0;
    }

    for (int i=0; i<*p; i++) {
        int x = random() % M;
        if (last[x] != 0) {
            free(last[x]);
            getThenIncrement(&fCount,1);
        }
        size_t sz = random() % 1000;
        last[x] = malloc(sz);
        getThenIncrement(&mCount,1);
        if (last[x] == 0) {
            Debug::say("failed to allocate %d",sz);
        }
    }

    //Debug::printf("checking %d\n",*p);

    for (int i=0; i<M; i++) {
        if (last[i] != 0) {
            free(last[i]);
            getThenIncrement(&fCount,1);
        }
    }

    //Debug::printf("finished %d\n",*p);

    *p = 0;
    done->sync();
}

#define N 100

int status[N];
    
void one(const char* name) {
    Debug::say("%s",name);
    int count = 0;
    done = new Barrier(N);

    for (int i=1; i<N; i++) {
        status[i] = i * 100;
        int id = threadCreate(f0,(void*)&status[i]);
        if (id == -1) {
            Debug::panic("can't create thread %d\n");
        }
        count++;
    }

    Debug::say("created %d",count);

    done->sync();

    Debug::say("checking output");
    for (int i=1; i<N; i++) {
        if (status[i] != 0) {
            Debug::say("thread failed %d, status[%d] = %d",i,i,status[i]);
        }
        count --;
    }

    Debug::say("final count %d",count);

    //delete done;
    done = nullptr;
}

void b0(void *arg) {
    if (arg != nullptr) Debug::say((char*)arg);
    done->sync();
}

void semUp(void* arg) {
    Semaphore *sem = (Semaphore*) arg;
    Debug::say("saying up");
    sem->up();
}

Semaphore *sem2 = nullptr;

void pingPong(void *arg) {
    Semaphore *sem = (Semaphore*) arg;
    sem->down();
    Debug::say("3");
    sem2->up();
}

Semaphore *sems[10];
Event *events[10];

void chain(void* arg) {
    int i = (int) arg;

    sems[i]->down();
    Debug::say("hello from %d",i);
    sems[i-1]->up();
}

void eventChain(void* arg) {
    int i = (int) arg;

    events[i]->wait();
    Debug::say("E%d",i);
    events[i-1]->signal();
}

BlockingLock *lock0;
BlockingLock *lock1;

void lockTest(void* arg) {
    lock0->lock();
    Debug::say("L2");
    lock1->unlock();
}

////////////

BoundedBuffer *out;
BoundedBuffer *in;

void bufferTest(void* arg) {
    int x = 0;
    while (true) {
        int v = out->get();
        if (v == -1) break;
        for (int i=0; i<v; i++) {
            void* p = malloc(i);
            free(p);
            x++;
        }
    }
    Debug::say("F8");
    in->put(x);
}

Config config;

extern "C" void kernelMain(void) {
    U8250 uart;
    Debug::init(&uart);
    Debug::debugAll = false;
    Debug::printf("\nHello, is there anybody out there?\n");

    /* discover configuration */
    configInit(&config);

    /* Initialize heap */
    heapInit((void*)0x100000,0x300000);

    /* initialize random number generator */
    randomInit(0);

    /* initialize threads */
    Thread::init();

    /* initialize idt */
    idtInit();

    /* initialize pic */
    picInit();

    /* initialize pit */
    pitInit(100000);

    /* enable interrupts */
    picEnable();

    ////////////////
    // Semaphores //
    ////////////////

    Debug::say("testing semaphores");
    Semaphore *sem = new Semaphore(1);
    sem->down();
    Debug::say("down works");

    Debug::say("creating a thread to say up");
    threadCreate(semUp,sem);
    sem->down();
    Debug::say("up worked");

    sem = new Semaphore(0);
    sem2 = new Semaphore(0);

    Debug::say("1");
    threadCreate(pingPong,sem);
    Debug::say("2");
    sem->up();
    sem2->down();
    Debug::say("4");

    for (int i=0; i<10; i++) {
        sems[i] = new Semaphore(0);
    }
    for (int i=1; i<10; i++) {
        threadCreate(chain,(void*)i);
    }
    sems[9]->up();
    sems[0]->down();
    Debug::say("chain is looking good");


    ////////////
    // Events //
    ////////////
        
    for (int i=0; i<10; i++) {
        events[i] = new Event();
    }
    for (int i=1; i<10; i++) {
        threadCreate(eventChain,(void*)i);
    }
    Debug::say("E10");
    events[9]->signal();
    events[0]->wait();
    Debug::say("E0");
    events[0]->wait();
    Debug::say("E0");

    //////////
    // Lock //
    //////////

    lock0 = new BlockingLock();
    lock1 = new BlockingLock();

    lock0->lock();
    lock1->lock();
    threadCreate(lockTest,nullptr);
    Debug::say("L1");
    lock0->unlock();
    lock1->lock();
    Debug::say("L3");
    
    /////////////
    // Barrier //
    /////////////

    Debug::say("test barriers");
    done = new Barrier(2);
    threadCreate(b0,(void*)"B0");
    done->sync();
    Debug::say("B1");

    Debug::say("Barrier(10)");
    done = new Barrier(10);
    for (int i=1; i<10; i++) {
        threadCreate(b0,nullptr);
    }
    done->sync();
    Debug::say("back");
    

    ///////////////////
    // BoundedBuffer //
    ///////////////////

    out = new BoundedBuffer(4);
    in = new BoundedBuffer(1);

    Debug::say("F1");
    out->put(10);
    Debug::say("F2");
    out->put(20);
    Debug::say("F3");
    out->put(30);
    Debug::say("F4");
    out->put(40);
    threadCreate(bufferTest,nullptr); 
    Debug::say("F5");
    out->put(50);
    Debug::say("F6");
    out->put(-1);
    Debug::say("F9");
    Debug::say("%d",in->get());
    Debug::say("F10");

    ///////////
    // Noise //
    ///////////

    Debug::say("going for the big one");
    one("---- phase1 ----");
    Debug::say("going for the big one, again");
    one("---- phase2 ----");

    if (mCount != fCount) {
        Debug::say("mCount %d",mCount);
        Debug::say("fCount %d",fCount);
    } else {
        Debug::say("counts match");
    }

    if (mCount < 500000) {
        Debug::say("mCount is too small %d",mCount);
    } else {
        Debug::say("mCount >= 500000");
    }
    
    threadExit();
}
