#ifndef _QUEUE_H_
#define _QUEUE_H_

template <class T>
class SimpleQueue {
    T* head;
    T* tail;    
public:
    SimpleQueue() : head(nullptr), tail(nullptr) {}

    void add(T* item) {
        item->next = nullptr;
        if (tail == nullptr) {
            head = item;
            tail = item;
        } else {
            tail->next = item;
            tail = item;
        }
    }

    T* remove() {
        T* ptr = head;
        if (ptr != nullptr) {
            head = ptr->next;
            if (head == nullptr) {
                tail = nullptr;
            }
        }
        return ptr;
    }
};

#endif
