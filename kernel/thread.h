#ifndef _THREAD_H_
#define _THREAD_H_

#include "stdint.h"

class SpinLock;

struct Thread {
    int id;
    Thread* next;
    uint32_t esp;
    uint32_t stack[1024];

    Thread(int id) : id(id), next(nullptr), esp((uint32_t)(&stack[1024])) {}

    void push(uint32_t v) {
        esp -= 4;
        *((uint32_t*) esp) = v;
    }

    static void init(void);
    static Thread* current(void);
};

typedef void (*ThreadFunc)(void*);

extern int threadCreate(ThreadFunc func, void* arg);
extern void threadBlock(SpinLock *lock, bool was);
extern void threadYield(void);
extern void threadExit(void);
extern void threadReady(Thread* t);
extern int threadId(void);

#endif
